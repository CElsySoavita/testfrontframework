<%-- 
    Document   : insererNouveau.jsp
    Created on : 16 nov. 2022, 22:14:16
    Author     : charles
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Affichage Personne</title>
    </head>
    <body>
        <h1>Affichage des informations entrées</h1>
        <p>Votre nom est <%= request.getAttribute("name")%></p>
        <p>Le nombre de vos enfant est <%= request.getAttribute("nombreenfant")%></p>
        <p>Votre date de naissance est le <%= request.getAttribute("datenaissance")%></p>
        <p><%
            if (Boolean.valueOf(request.getAttribute("en_vie").toString())) {
                out.print("Encore en vie");
            } else {
                out.print("Déjà Mort");
            }
            %>
        </p>

    </body>
</html>
