/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.sql.Date;
import java.util.HashMap;
import util.ModelView;
import util.Url;

/**
 *
 * @author charles
 */
public class Person {

    String name;
    Integer nombreenfant;
    Date datenaissance;
    Boolean en_vie;

    public Date getDatenaissance() {
        return datenaissance;
    }

    public void setDatenaissance(Date datenaissance) {
        this.datenaissance = datenaissance;
    }
    
    public Boolean getEn_vie() {
        return en_vie;
    }

    public void setEn_vie(Boolean en_vie) {
        this.en_vie = en_vie;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNombreenfant() {
        return nombreenfant;
    }

    public void setNombreenfant(Integer nombreenfant) {
        this.nombreenfant = nombreenfant;
    }

  

    @Url(name = "aff-pers")
    public ModelView afficherPers() {
        ModelView mv = new ModelView();
        HashMap hm = new HashMap();
        hm.put("name", name);
        hm.put("nombreenfant", nombreenfant);
        hm.put("datenaissance", datenaissance);
        hm.put("en_vie", en_vie);
        mv.setData(hm);
        mv.setJspPage("afficherpersonne.jsp");
        return mv;
    }

    @Url(name = "ent-pers")
    public ModelView entreePers() {
        ModelView mv = new ModelView();
        mv.setJspPage("entreepersonne.jsp");
        return mv;
    }
}
