<%-- 
    Document   : delNouveau
    Created on : 16 nov. 2022, 22:15:33
    Author     : charles
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Entree Personne</title>
    </head>
    <body>
        <h1>Entree les informations d'une personne</h1>
        <form action="aff-pers.do" method="post">
            <p> Name <input type="text" name="name"> </p>
            <p> Nombre d'Enfant <input type="number" name="nombreenfant"> </p>
            <p> Date de Naissance <input type="date" name="datenaissance"> </p>            
            
            <p> En Vie 
                <select name="en_vie">
                    <option value="true">True</option>
                    <option value="false">False</option>
                </select>

            </p>

            <p> <input type="submit" value="Valider"> </p>
        </form>
    </body>
</html>
